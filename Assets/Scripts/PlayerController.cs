﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed;
	public Text textCount;
	public Text textWin;

	private Rigidbody rb;
	private int count;

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
		count = 0;
		textWin.text = "";

		UpdateCountText ();
	}
	// Use this for initialization
	void FixedUpdate () 
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
	}

	// Trigger handler
	void OnTriggerEnter(Collider other)
	{
		// Set the other object to false
		if (other.gameObject.CompareTag ("Pick Up Object")) 
		{
			other.gameObject.SetActive (false);
			count+=1;
			UpdateCountText ();

			if(count == 11)
			{
				textWin.text = "YOU WIN!!";
			}
		}
	}

	void UpdateCountText()
	{
		textCount.text = "Count : " + count;
	}

}